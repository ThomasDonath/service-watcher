package checkers

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ThomasDonath/service-watcher/logger"
)

func TestURLTest(t *testing.T) {
	logger.SetDebug(true)

	assert := assert.New(t)

	err := URLTest("http://www.google.de", 200)
	assert.NoError(err)

	err = URLTest("https://www.google.de", 200)
	assert.NoError(err)

	err = URLTest("https://www.google.de", 300)
	assert.Equal(errors.New("expected response code 300 but got 200"), err)

	err = URLTest("https://aHostThatNotExists", 300)
	assert.Error(err)
}
