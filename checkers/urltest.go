package checkers

import (
	"fmt"
	"net/http"

	"gitlab.com/ThomasDonath/service-watcher/logger"
)

// URLTest checks the response code for the URL
func URLTest(url string, resCode int) error {
	logger.Debug.Println(fmt.Sprintf("entering URLTest() for %s", url))

	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	if resp.StatusCode != resCode {
		return fmt.Errorf("expected response code %d but got %d", resCode, resp.StatusCode)
	}

	logger.Debug.Println("leaving URLTest()")
	return nil
}
