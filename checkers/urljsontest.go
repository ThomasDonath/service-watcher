package checkers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/ThomasDonath/service-watcher/logger"
)

// TODO: enhance to use values in nested properties too

// URLJsonTest checks the returned JSON to have a root property with the given value
func URLJsonTest(url string, rootProperty string, shouldBeValue string) error {
	logger.Debug.Println(fmt.Sprintf("entering URLJsonTest() for %s", url))

	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		logger.Error.Printf("error response %v", resp.Status)
		return fmt.Errorf("error response" + resp.Status)
	}

	//	fmt.Println(body)
	var buff interface{}
	err = json.Unmarshal(body, &buff)
	if err != nil {
		logger.Error.Println("error unmarshaling response", err)
		return fmt.Errorf("error unmarshaling response")
	}

	//logger.Debug.Println(buff)
	//https://blog.golang.org/json-and-go

	val := buff.(map[string]interface{})[rootProperty]
	logger.Debug.Println(fmt.Sprintf("result %v", val))

	if val != shouldBeValue {
		logger.Error.Printf("wrong value in json property %s", val)
		return fmt.Errorf("wrong value in json property %s", val)
	}

	logger.Debug.Println("leaving URLJsonTest()")
	return nil
}
