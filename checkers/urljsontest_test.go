package checkers

import (
	"context"
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ThomasDonath/service-watcher/logger"
)

func jsonHandlerUp(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, `{"status":"UP","diskSpace":{"status":"UP","total":42242113536,"free":29330878464,"threshold":10485760},"db":{"status":"UP","database":"Oracle","hello":"Hello"},"refreshScope":{"status":"UP"},"hystrix":{"status":"UP"}}`)
}
func jsonHandlerDown(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, `{"status":"DOWN","diskSpace":{"status":"UP","total":42242113536,"free":29330878464,"threshold":10485760},"db":{"status":"UP","database":"Oracle","hello":"Hello"},"refreshScope":{"status":"UP"},"hystrix":{"status":"UP"}}`)
}

func testServer() *http.Server {
	http.HandleFunc("/test-up", jsonHandlerUp)
	http.HandleFunc("/test-down", jsonHandlerDown)

	srv := &http.Server{
		Addr:           ":8080",
		Handler:        nil,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	go srv.ListenAndServe()

	return srv
}

func TestURLJsonTest(t *testing.T) {
	logger.SetDebug(true)

	srv := testServer()

	assert := assert.New(t)

	err := URLJsonTest("http://localhost:8080/test", "egal", "egal")
	assert.Error(err)

	err = URLJsonTest("http://localhost:8080/test-up", "status", "UP")
	assert.NoError(err)

	err = URLJsonTest("http://localhost:8080/test-down", "status", "UP")
	assert.Error(err)

	srv.Shutdown(context.Background())
}
