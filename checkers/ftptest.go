package checkers

import (
	"fmt"

	"github.com/dutchcoders/goftp"
	"gitlab.com/ThomasDonath/service-watcher/logger"
)

// FTPTest tests a connection to the FTP server
func FTPTest(hostPort string) error {
	logger.Debug.Println(fmt.Sprintf("entering FTPTest() for %s", hostPort))

	var err error
	var ftp *goftp.FTP

	if ftp, err = goftp.Connect(hostPort); err != nil {
		return (err)
	}

	defer ftp.Close()

	logger.Debug.Println("leaving FTPTest()")
	return nil
}
