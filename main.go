package main

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/ThomasDonath/service-watcher/checkers"
	"gitlab.com/ThomasDonath/service-watcher/configuration"
	"gitlab.com/ThomasDonath/service-watcher/logger"
	"gitlab.com/ThomasDonath/service-watcher/model"
)

func healthState(w http.ResponseWriter, r *http.Request) {
	logger.Debug.Println(`Health Service asked`)

	var iAmHealthy = true
	var healthMessage = ""

	//	testsList := configuration.GetTestsFromFile("./ressources_test/tests.json")
	testsList := configuration.GetTestsFromEnv()

	for i := range testsList {
		logger.Debug.Println("processing", i, testsList[i].TestName, testsList[i].TestType)

		switch testsList[i].TestType {
		case model.TestTypeURL:
			err := checkers.URLTest(testsList[i].URL, testsList[i].ResponseCode)
			if err != nil {
				iAmHealthy = false
				healthMessage = fmt.Sprintf("%s\n%s Fehler: %s", healthMessage, testsList[i].Message, err)
			}
		case model.TestTypeJSON:
			err := checkers.URLJsonTest(testsList[i].URL, testsList[i].JSONPath, testsList[i].JSONValue)
			if err != nil {
				iAmHealthy = false
				healthMessage = fmt.Sprintf("%s\n%s Fehler: %s", healthMessage, testsList[i].Message, err)
			}
		case model.TestTypeFtp:
			err := checkers.FTPTest(testsList[i].URL)
			if err != nil {
				iAmHealthy = false
				healthMessage = fmt.Sprintf("%s\n%s Fehler: %s", healthMessage, testsList[i].Message, err)
			}
		default:
			logger.Error.Println("invalid test type", testsList[i].TestName, testsList[i].TestType)
			healthMessage = fmt.Sprintf("%s\n%s invalid test type: %s", healthMessage, testsList[i].TestName, testsList[i].TestType)
		}
	}
	logger.Debug.Printf("Health Status %t\n", iAmHealthy)

	if iAmHealthy {
		w.WriteHeader(200)
		fmt.Fprintf(w, "Service Watcher has successfully tested: \n")
		for i := range testsList {
			fmt.Fprintf(w, "%s (%s)\n", testsList[i].TestName, testsList[i].URL)
		}
	} else {
		w.WriteHeader(599)
		fmt.Fprintf(w, "Service Watcher detected these errors: %s\n", healthMessage)
	}
}

func httpServer() {
	http.HandleFunc("/", healthState)

	srv := &http.Server{
		Addr:           ":8080",
		Handler:        nil,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	srv.ListenAndServe()
}

func main() {
	logger.SetDebug(!true) // TODO get from command line
	logger.Info.Println("starting service watchter")

	httpServer()
}
