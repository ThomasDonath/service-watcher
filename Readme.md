# Service Watcher

## What is it

A simple server checking a list of given services:

* URL test == HTTP get returns the given response code
* URL JSON == HTTP get, should return 200 and a JSON body, a (root) property inside the JSON response should have a given value
* FTP == open connection to FTP server
* TODO RabbitMQ length of the queue must not greater than the given value

1. if a HTTP request comes in, server will read the properties and execute the tests
1. If a test fails the server will respond with HTTP599 #TODO!
1. If all tests are successfull server returns 200 (ok) and a list of all tests.

List of tests comes as JSON from env variable SW_TESTS.

There is another function to read this from file directly. To use it change the call in main.go.

## build it

```shell
go get
go run main.go
#export GOOS=linux | windows

# important for Docker
export GOARCH=386
go build

cp service-watcher docker/.
cp ressources_test/tests.json docker/.

cd docker
docker build -t sw .
docker run --rm -p 8080:8080 --add-host devbox2:$(hostname -i) -e "SW_TESTS=$(cat ../ressources_test/tests.json)" sw
```

## test it

```shell
# run FTP server
docker start myftp

# install json-server
# sudo npm install -g json-server
json-server --watch ressources_test/json-server-data.json


export SW_TESTS=$(cat ressources_test/tests.json)
```
