package model

type TestDef struct {
	TestName     string `json:"testName"`
	TestType     string `json:"testType"`
	URL          string `json:"URL"`
	ResponseCode int    `json:"responseCode"`
	JSONPath     string `json:"JsonPath"`
	JSONValue    string `json:"JsonValue"`
	Message      string `json:"Message"`
}

const (
	TestTypeURL    = "HTTP"
	TestTypeJSON   = "HTTP JSON"
	TestTypeFtp    = "FTP"
	TestTypeRabbit = "HTTP Rabbit Queue"
)

