package configuration

import (
	"fmt"
	"os"

	"gitlab.com/ThomasDonath/service-watcher/logger"
	"gitlab.com/ThomasDonath/service-watcher/model"
)

// GetTestsFromEnv reads configuration from os env and return it
func GetTestsFromEnv() []model.TestDef {
	logger.Debug.Println("entering getTests()")

	raw := os.Getenv("SW_TESTS")
	if raw == "" {
		fmt.Println("Environment Variable not set")
		os.Exit(1)
	}

	return getTests([]byte(raw))
}
