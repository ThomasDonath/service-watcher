package configuration

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ThomasDonath/service-watcher/logger"
	"gitlab.com/ThomasDonath/service-watcher/model"
)

// TestReadJSON testing read from JSON file
func TestGetTestsFromFile(t *testing.T) {
	logger.SetDebug(true)

	assert := assert.New(t)

	testListFile := GetTestsFromFile("../ressources_test/tests.json")

	assert.Equal(len(testListFile), 6, "check length of arry")
	assert.Equal(testListFile[0].TestType, model.TestTypeURL, "check type of 1. test")
	assert.Equal(testListFile[0].URL, "http://www.google.com", "check URL of 1. test")
	assert.Equal(testListFile[0].ResponseCode, 200)
	//	assert.Equal(testList[0].JSONPath, "", "check type of 1. test")
	//	assert.Equal(testList[0].Message, "", "check type of 1. test")

	assert.Equal(testListFile[1].TestType, model.TestTypeJSON, "check type of 2. test")
	assert.Equal(testListFile[2].TestType, model.TestTypeURL, "check type of 3. test")
	assert.Equal(testListFile[3].TestType, model.TestTypeJSON, "check type of 4. test")
	assert.Equal(testListFile[4].TestType, model.TestTypeRabbit, "check type of 5. test")
}
