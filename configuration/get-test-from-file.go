package configuration

import (
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/ThomasDonath/service-watcher/logger"
	"gitlab.com/ThomasDonath/service-watcher/model"
)

// GetTests reads configuration from file and return it
func GetTestsFromFile(filename string) []model.TestDef {
	logger.Debug.Println("entering GetTestsFromFile()")

	raw, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	return getTests(raw)
}
