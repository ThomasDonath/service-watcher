package configuration

import (
	"encoding/json"
	"fmt"

	"gitlab.com/ThomasDonath/service-watcher/logger"
	"gitlab.com/ThomasDonath/service-watcher/model"
)

// GetTests reads configuration from file and return it
func getTests(jsonContent []byte) []model.TestDef {
	logger.Debug.Println("entering getTests()")

	tl := make([]model.TestDef, 3)

	json.Unmarshal(jsonContent, &tl)

	logger.Debug.Println(fmt.Sprintf("leaving getTests(), length %d\n", len(tl)))
	return tl
}
