package logger

import (
	"bytes"
	"log"
	"os"
)

var (
	nullOut bytes.Buffer

	Debug   = log.New(&nullOut, "DEBUG: ", log.Ldate|log.Ltime|log.Lshortfile)
	Info    = log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime)
	Warning = log.New(os.Stdout, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	Error   = log.New(os.Stderr, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
)

func SetDebug(isDebug bool) {
	if isDebug {
		Debug = log.New(os.Stdout, "DEBUG: ", log.Ldate|log.Ltime|log.Lshortfile)
	} else {
		Debug = log.New(&nullOut, "DEBUG: ", log.Ldate|log.Ltime|log.Lshortfile)
	}
}
